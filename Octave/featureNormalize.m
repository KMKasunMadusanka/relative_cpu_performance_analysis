function [X_norm, mu, sigma] = featureNormalize(X)

% Initialize some useful values
X_norm = X;
mu = zeros(1, size(X, 2));
sigma = zeros(1, size(X, 2));
      
 
  for k = 1:size(X, 2)
    mu(:,k) = mean(X_norm(:,k));
    sigma(:,k) = std(X_norm(:,k));
    X_norm(:,k) = (X_norm(:,k) - mu(:,k)) / sigma(:,k);
  end
 
 
end
