% clean exsisting Figures
clear ;
close all;
clc;

fprintf('Loading Relative CPU Performance Data!!!\n');

% load data to variable using Text file.
data = csvread('data.txt');

X = data(:, 3:8);
y = data(:, 10);
m = length(y);

% Print sample output
fprintf('First 10 data rows from the dataset: \n');
fprintf(' x = [%.0f %.0f %.0f %.0f %.0f %.0f], y = %.0f \n', [X(1:10,:) y(1:10,:)]');


%% ================ Part 1: Feature Normalizaation ================

% Scale features and set them to zero mean
fprintf('Starting Normalizing Features ...\n');

[X mu sigma] = featureNormalize(X);

% Add intercept term to X
X = [ones(m, 1) X];
% =================================================================

%% ================ Part 2: Gradient Descent ================

fprintf('Running gradient descent ...\n');

% Choose some alpha value
alpha = 0.0465;
num_iters = 210;

% Init Theta and Run Gradient Descent 
theta = zeros(7, 1);
[theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters);

% Plot the convergence graph
figure;
plot(1:numel(J_history), J_history, '-b', 'LineWidth', 3);
xlabel('Number of iterations');
ylabel('Cost (J)');

% Display gradient descent's result
fprintf('Theta computed from gradient descent: \n');
fprintf(' %f \n', theta);
fprintf('\n');
% ============================================================
fprintf('Program paused. Press enter to continue.\n');
pause;

% ====================== Part 3: Sample Prediction ======================

prediction = [1 ((23 - mu(:,1))/sigma(:,1)) ((16000 - mu(:,2))/sigma(:,2)) ((32000 - mu(:,3))/sigma(:,3)) ((64 - mu(:,4))/sigma(:,4)) ((16 - mu(:,5))/sigma(:,5)) ((32 - mu(:,6))/sigma(:,6))];
price = prediction * theta;



fprintf(['Predicted  MYCT:23 MMIN:16000 MMAX:32000 CACH:64 CHMIN:16 CHMAX: 32  Machine ' ...
         '(using gradient descent):\n estimated relative performance from the original article :%f\n'], price);
% =======================================================================

fprintf('End.\n');


