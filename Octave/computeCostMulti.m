function J = computeCostMulti(X, y, theta)

% Initialize some useful values
m = length(y); 

difference = X * theta - y;
vector = difference' * difference;
J = (1 / (2 * m)) * vector;


end
